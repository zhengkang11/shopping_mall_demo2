import Vue from 'vue'
import App from './App'

import store from './store'
import http from './common/http'
import sui from './common/sui'

import cuCustom from './colorui/components/cu-custom.vue'



Vue.config.productionTip = false
Vue.prototype.sui = sui
Vue.prototype.http = http
Vue.prototype.$eventHub = Vue.prototype.$eventHub || new Vue()
Vue.prototype.$store = store

Vue.component('cu-custom', cuCustom)

App.mpType = 'app'

const app = new Vue({
	store,
	...App
})
app.$mount()
