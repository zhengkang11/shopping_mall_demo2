import Vue from 'vue'
import Vuex from 'vuex'
import http from '../common/http.js'

Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		version: "1.0.0",
		isLogin: uni.getStorageSync("token") ? true : false,
		openid: '',
		uid: 0,
		userInfo: {
			name: ''
		},
	},
	mutations: {
		login(state, payload) { //登录成功
			console.log(state);
			// if (payload) {
			// 	state.mobile = payload.mobile;
			// 	state.memberId = payload.memberId;
			// }
			state.isLogin = true;
		},
		logout(state) { //退出
			state.uid = 0;
			state.isLogin = false;
		},
	}
})

export default store
