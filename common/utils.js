const md5 = require('./md5.js');
const debug = true; //是否开发模式
const appkey = '7dab9e34-7c7b-4948-923b-28d0ecbe64f8'; //api密钥
const sid = 'AA829A27F469550A74A2E672282AAB53'; //商家ID
/**获取*/
const getSid = function() {
	return sid;
}
/**时间戳 */
const getTimestamp = function() {
	return Math.round(new Date().getTime() / 1000);
}
/** 随机字符串，默认32位*/
const getNonce = function(length) {
	if (length == null) length = 32;
	var str = "",
		pos = 0,
		arr = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
			'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
			'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
		];

	for (var i = 0; i < length; i++) {
		pos = Math.round(Math.random() * (arr.length - 1));
		str += arr[pos];
	}
	return str;
}
/**签名 */
const sign = function(queryData, timestamp, nonce) {
	var data = orderWithData(queryData);

	log('sign data：' + data);
	log('sign appkey：' + appkey);
	log('sign nonce：' + nonce);

	log('sign timestamp：' + timestamp);
	return md5.md5(timestamp + nonce + appkey + data);
}
/**将参数进行排序 */
let orderWithData = function(dic) {
	if (dic == null) return '';
	var result = "";
	var sdic = Object.keys(dic).sort(function(a, b) {
		return a.localeCompare(b)
	});
	var value = "";
	for (var ki in sdic) {
		if (dic[sdic[ki]] == null) {
			value = ""
		} else {
			value = dic[sdic[ki]];
			if (typeof value === 'object') value = JSON.stringify(value);
		}
		result += sdic[ki] + value;
	}
	return result.replace(/\s/g, "");
}
const utils = {
	toast: function(tips) {
		uni.showToast({
			title: tips || "出错啦~",
			icon: 'none',
			duration: 2000
		})
	}
};
//输出
module.exports = {
	debug: debug,
	md5: md5.md5,
	toast: utils.toast
};