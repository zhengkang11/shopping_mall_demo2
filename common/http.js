//公共js
const md5 = require('common/md5.js');
//变量
const host = "http://localhost:28023"; //接口地址
const app_key = '7dab9e34-7c7b-4948-923b-28d0ecbe64f8'; //api密钥
const sid = 'AA829A27F469550A74A2E672282AAB53'; //商家ID,

const MSHttp = {
	getTimestamp: function() {
		return Math.round(new Date().getTime() / 1000);
	},
	getNonce: function(length) {
		if (length == null) length = 32;
		var str = "",
			pos = 0,
			arr = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
				'l',
				'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
				'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
			];

		for (var i = 0; i < length; i++) {
			pos = Math.round(Math.random() * (arr.length - 1));
			str += arr[pos];
		}
		return str;
	},
	getOrderWithData: function(dic) {
		if (dic == null) return '';
		var result = "";
		var sdic = Object.keys(dic).sort(function(a, b) {
			return a.localeCompare(b)
		});
		var value = "";
		for (var ki in sdic) {
			if (dic[sdic[ki]] == null) {
				value = ""
			} else {
				value = dic[sdic[ki]];
				if (typeof value === 'object') value = JSON.stringify(value);
			}
			result += sdic[ki] + value;
		}
		return result.replace(/\s/g, "");
	},
	getSign: function(queryData, timestamp, nonce) {
		var data = MSHttp.getOrderWithData(queryData);
		return md5.md5(timestamp + nonce + app_key + data);
	},
	setToken(token) {
		uni.removeStorageSync('token');
	},
	getToken() {
		return uni.getStorageSync("token")
	},
	clearToken() {
		uni.setStorageSync("token", token)
	},
	request: function(url, data, method, type, showLoading) {
		if (showLoading) {
			uni.showLoading({
				mask: true,
				title: '请稍候...'
			})
		}
		if (data) delete data["token"];
		let timestamp = MSHttp.getTimestamp(); //时间截
		let nonce = MSHttp.getNonce(); //随机字符串
		let signature = MSHttp.getSign(data, timestamp, nonce); //签名
		let token = MSHttp.getToken();

		var header = {
			'content-type': type == 'form' ? 'application/x-www-form-urlencoded' : 'application/json',
			'timestamp': timestamp,
			'nonce': nonce,
			'signature': signature,
			'token': token || "",
			'sid': sid
		};
		const params = data ? {
			data: method === "POST" ? data : JSON.stringify(data)
		} : null;
		return new Promise((resolve, reject) => {
			let config = {
				url: url,
				header: header,
				timeout: 10000,
				method: method, //'GET','POST'
				dataType: 'json',
				success: (res) => {
					showLoading && uni.hideLoading();
					if (res.statusCode == 200) {
						if (res.data.flag == false && res.data.msg == 'api token unauthorized.') {
							MSHttp.clearToken();
						} else if (res.header['api-token']) {
							MSHttp.setToken(res.header['api-token']);
						}
						resolve(res.data);
					} else {
						resolve({
							flat: false,
							msg: res.data.exceptionmessage
						});
					}
				},
				fail: (res) => {
					showLoading && uni.hideLoading();
					if (res.errMsg == "request:fail abort") return;
					reject({
						flag: false,
						msg: '网络不给力，请稍后再试.'
					});
				}
			};
			if (params) config.data = params;
			uni.request(config);
		})
	},
	url(url) {
		return host + url;
	},
	apiUrl(url) {
		return MSHttp.url('/api' + url);
	},
	webUrl(url) {
		return MSHttp.url('/h5' + url);
	},
	apiGet(url, data, fn, showLoading) {
		let http = MSHttp.request(MSHttp.apiUrl(url), data, 'GET', null, showLoading);
		http.then(fn).catch(fn);
	},
	apiPost(url, data, fn, showLoading) {
		let http = MSHttp.request(MSHttp.apiUrl(url), data, 'POST', null, showLoading);
		http.then(fn).catch(fn);
	}
}

module.exports = {
	// request: MSHttp.request,
	apiGet: MSHttp.apiGet,
	apiPost: MSHttp.apiPost,
}
